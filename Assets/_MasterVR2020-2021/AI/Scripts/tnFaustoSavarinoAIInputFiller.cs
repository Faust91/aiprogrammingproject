﻿using BehaviorDesigner.Runtime;
using BehaviorDesigner.Runtime.Tasks;
using System.Collections;
using System.Collections.Generic;
using TuesdayNights;
using UnityEngine;

[System.Serializable]
public class SharedInputFiller : SharedVariable<tnFaustoSavarinoAIInputFiller>
{
    public static implicit operator SharedInputFiller(tnFaustoSavarinoAIInputFiller value) { return new SharedInputFiller { Value = value }; }
}

[System.Serializable]
public class tnFaustoSavarinoAIInputFiller : tnStandardAIInputFillerBase
{

    public class IsBallInMyHalfSideCondition : Conditional
    {
        public SharedInputFiller filler;

        public override TaskStatus OnUpdate()
        {
            if (((tnFaustoSavarinoAIInputFiller)filler.GetValue()).IsBallInMyHalfSide())
            {
                return TaskStatus.Success;
            }
            else
            {
                return TaskStatus.Failure;
            }
        }
    }

    public class IsCharacterBehindBallCondition : Conditional
    {
        public SharedInputFiller filler;
        public SharedGameObject character;

        public override TaskStatus OnUpdate()
        {
            if (((tnFaustoSavarinoAIInputFiller)filler.GetValue()).IsCharacterBehindBall_Internal(((GameObject)character.GetValue()).transform))
            {
                return TaskStatus.Success;
            }
            else
            {
                return TaskStatus.Failure;
            }
        }
    }

    public class IsInMyHalfSideCondition : Conditional
    {
        public SharedInputFiller filler;
        public SharedTransform target;

        public override TaskStatus OnUpdate()
        {
            if (((tnFaustoSavarinoAIInputFiller)filler.GetValue()).IsInMyHalfSide((Transform)target.GetValue()))
            {
                return TaskStatus.Success;
            }
            else
            {
                return TaskStatus.Failure;
            }
        }
    }

    public class CheckEnergyCondition : Conditional
    {
        public SharedInputFiller filler;
        public SharedFloat minEnergy;

        public override TaskStatus OnUpdate()
        {
            bool ret = ((tnFaustoSavarinoAIInputFiller)filler.GetValue()).CheckEnergy((float)minEnergy.GetValue());
            if (ret)
            {
                return TaskStatus.Success;
            }
            else
            {
                return TaskStatus.Failure;
            }
        }
    }

    public class UpdateTackleCondition : Conditional
    {
        public SharedInputFiller filler;
        public SharedTransform o_Opponent;
        public SharedFloat m_TackleRadius;
        public SharedFloat m_BallDistanceThreshold;

        public override TaskStatus OnUpdate()
        {
            Transform o_OpponentTemp = (Transform)o_Opponent.GetValue();

            bool ret = ((tnFaustoSavarinoAIInputFiller)filler.GetValue()).UpdateTackle_Internal(out o_OpponentTemp,
                (float)m_TackleRadius.GetValue(), (float)m_BallDistanceThreshold.GetValue());

            o_Opponent.SetValue(o_OpponentTemp);

            if (ret)
            {
                return TaskStatus.Success;
            }
            else
            {
                return TaskStatus.Failure;
            }
        }
    }

    private bool UpdateTackle_Internal(out Transform o_Opponent, float m_TackleRadius, float m_BallDistanceThreshold)
    {
        // Try to push away other characters.

        Transform nearestOpponent = GetOpponentNearestTo(self);

        o_Opponent = nearestOpponent;

        bool tackleRequested = false;

        Vector2 nearestOpponetPos = nearestOpponent.position;
        float distance = Vector2.Distance(myPosition, nearestOpponetPos);

        if (distance < m_TackleRadius)
        {
            if (ballDistance > m_BallDistanceThreshold)
            {
                tackleRequested = true;
            }
        }

        return tackleRequested;
    }

    public class UpdateAttractCondition : Conditional
    {
        public SharedInputFiller filler;
        public SharedVector2 o_Axes;
        public SharedFloat m_MinAttractEnergy;
        public SharedFloat m_AttractCooldownTimer;
        public SharedFloat m_AttractTimer;
        public SharedFloat m_AttractTimeThreshold;
        public SharedFloat m_AttractMaxRadius;
        public SharedFloat m_AttractMinRadius;
        public SharedBool m_IsAttracting;
        public SharedFloat m_AttractCooldown;

        public override TaskStatus OnUpdate()
        {
            Vector2 o_AxesTemp = (Vector2)o_Axes.GetValue();
            bool o_IsAttracting;
            float o_AttractCooldownTimer;

            bool ret = ((tnFaustoSavarinoAIInputFiller)filler.GetValue()).UpdateAttract_Internal(
                out o_AxesTemp, out o_IsAttracting, out o_AttractCooldownTimer,
                (float)m_MinAttractEnergy.GetValue(), (float)m_AttractCooldownTimer.GetValue(),
                (float)m_AttractTimer.GetValue(), (float)m_AttractTimeThreshold.GetValue(),
                (float)m_AttractMaxRadius.GetValue(), (float)m_AttractMinRadius.GetValue(),
                (bool)m_IsAttracting.GetValue(), (float)m_AttractCooldown.GetValue());

            o_Axes.SetValue(o_AxesTemp);
            m_IsAttracting.SetValue(o_IsAttracting);
            m_AttractCooldownTimer.SetValue(o_AttractCooldownTimer);

            if (ret)
            {
                return TaskStatus.Success;
            }
            else
            {
                return TaskStatus.Failure;
            }
        }
    }

    private bool UpdateAttract_Internal(out Vector2 o_Axes,
        out bool o_IsAttracting, out float o_AttractCooldownTimer,
        float m_MinAttractEnergy, float m_AttractCooldownTimer,
        float m_AttractTimer, float m_AttractTimeThreshold,
        float m_AttractMaxRadius, float m_AttractMinRadius,
        bool m_IsAttracting, float m_AttractCooldown)
    {
        o_Axes = Vector2.zero;
        o_IsAttracting = m_IsAttracting;
        o_AttractCooldownTimer = m_AttractCooldownTimer;

        bool isAttracting = false;

        if (CheckEnergy(m_MinAttractEnergy))
        {
            if (m_AttractCooldownTimer == 0f && (m_AttractTimer < m_AttractTimeThreshold))
            {
                if (!IsBehindBall(self) && ballDistance < m_AttractMaxRadius)
                {
                    if (ballDistance < m_AttractMinRadius)
                    {
                        Vector2 attractDirection = Vector2.zero;
                        bool forcedDirection = false;

                        if (myGoalPosition.x < midfield.x)
                        {
                            if (!IsCloseToRightBorder(ball, 0.1f))
                            {
                                if (IsCloseToLeftBorder(ball, 0.1f))
                                {
                                    attractDirection += Vector2.right;
                                    forcedDirection = true;
                                }

                                if (IsCloseToTopBorder(ball, 0.1f))
                                {
                                    attractDirection += Vector2.down;
                                    forcedDirection = true;
                                }

                                if (IsCloseToBottomBorder(ball, 0.1f))
                                {
                                    attractDirection += Vector2.up;
                                    forcedDirection = true;
                                }
                            }
                        }

                        if (myGoalPosition.x > midfield.x)
                        {
                            if (!IsCloseToLeftBorder(ball, 0.1f))
                            {
                                if (IsCloseToRightBorder(ball, 0.1f))
                                {
                                    attractDirection += Vector2.left;
                                    forcedDirection = true;
                                }

                                if (IsCloseToTopBorder(ball, 0.1f))
                                {
                                    attractDirection += Vector2.down;
                                    forcedDirection = true;
                                }

                                if (IsCloseToBottomBorder(ball, 0.1f))
                                {
                                    attractDirection += Vector2.up;
                                    forcedDirection = true;
                                }
                            }
                        }

                        if (!forcedDirection)
                        {
                            Vector2 myGoalDirection = GetMyGoalDirection(self);

                            Vector2 ballDirection = GetBallDirection(self);
                            Vector2 axisA = new Vector2(ballDirection.y, -ballDirection.x);
                            Vector2 axisB = -axisA;

                            float dotA = Vector2.Dot(axisA, myGoalDirection);
                            float dotB = Vector2.Dot(axisB, myGoalDirection);

                            if (dotA > dotB)
                            {
                                attractDirection = axisA;
                            }
                            else // dotB > dotA
                            {
                                attractDirection = axisB;
                            }
                        }

                        o_Axes = attractDirection; // The ball is very close to you, you can start moving with it attached.
                    }
                    else
                    {
                        o_Axes = Vector2.zero;
                    }

                    isAttracting = true;

                    SetBehaviourType_Internal("Attract");
                }
            }
        }

        if (!isAttracting && m_IsAttracting)
        {
            m_AttractCooldownTimer = m_AttractCooldown;
        }

        m_IsAttracting = isAttracting;

        o_IsAttracting = m_IsAttracting;
        o_AttractCooldownTimer = m_AttractCooldownTimer;

        return m_IsAttracting;
    }

    public class UpdateDefenderAction : Action
    {
        public SharedInputFiller filler;
        public SharedVector2 o_Axes;
        public SharedBool o_KickButton;
        public SharedBool o_DashButton;

        public SharedFloat m_ForcedDashDistance;
        public SharedFloat m_MinFleeDistanceFactor;
        public SharedFloat m_MaxFleeDistanceFactor;
        public SharedFloat m_SeparationThreshold;
        public SharedFloat m_RecoverTimer;
        public SharedFloat m_RecoverTimeThreshold;
        public SharedFloat m_DashDistance;
        public SharedFloat m_KickPrecision;

        public override TaskStatus OnUpdate()
        {
            Vector2 o_AxesTemp = (Vector2)o_Axes.GetValue();
            bool o_KickButtonTemp = (bool)o_KickButton.GetValue();
            bool o_DashButtonTemp = (bool)o_DashButton.GetValue();

            ((tnFaustoSavarinoAIInputFiller)filler.GetValue()).UpdateDefender_Internal(
                out o_AxesTemp, out o_KickButtonTemp, out o_DashButtonTemp,
                (float)m_ForcedDashDistance.GetValue(), (float)m_MinFleeDistanceFactor.GetValue(), (float)m_MaxFleeDistanceFactor.GetValue(),
                (float)m_SeparationThreshold.GetValue(), (float)m_RecoverTimer.GetValue(), (float)m_RecoverTimeThreshold.GetValue(),
                (float)m_DashDistance.GetValue(), (float)m_KickPrecision.GetValue());

            o_Axes.SetValue(o_AxesTemp);
            o_KickButton.SetValue(o_KickButtonTemp);
            o_DashButton.SetValue(o_DashButtonTemp);

            return TaskStatus.Success;
        }
    }

    private void UpdateDefender_Internal(out Vector2 o_Axes, out bool o_KickButton, out bool o_DashButton,
        float m_ForcedDashDistance, float m_MinFleeDistanceFactor, float m_MaxFleeDistanceFactor, float m_SeparationThreshold,
        float m_RecoverTimer, float m_RecoverTimeThreshold, float m_DashDistance, float m_KickPrecision)
    {
        o_Axes = Vector2.zero;

        o_KickButton = false;
        o_DashButton = false;

        // You're a defender.

        if (!IsBallInMyHalfSide())
        {
            if (!IsCharacterBehindBall(self))
            {
                // You're a defender and you're not behind the ball. Return through your goal.

                Recover(myGoalPosition, out o_Axes, out o_KickButton, out o_DashButton, m_ForcedDashDistance);

                SetBehaviourType_Internal("Recover");
            }
            else
            {
                Vector2 defenderPosition = referencePosition;

                Vector2 axes = SeekPosition(defenderPosition, colliderRadius, m_MinFleeDistanceFactor, m_MaxFleeDistanceFactor, m_SeparationThreshold); // Ball is on opponent side and you're behind the ball. WAIT.
                o_Axes = axes;

                SetBehaviourType_Internal("Wait");
            }
        }
        else
        {
            // The ball is on your side - your controlled area.

            if (IsCharacterBehindBall(self))
            {
                // I'm behind the ball. If I'm the nearest to the ball, I'll try to kick it away. Otherwise, check if you must protect the goal.

                Transform nearestTeamateToBall = GetTeammateBehindBallNearestTo(ball, true);
                if (nearestTeamateToBall == self.transform)
                {
                    ChargeBall(out o_Axes, out o_KickButton, out o_DashButton, m_RecoverTimer, m_RecoverTimeThreshold, m_DashDistance, m_KickPrecision);

                    SetBehaviourType_Internal("ChargeBall");
                }
                else
                {
                    // I'm not the nearest player to the ball. Maybe I must protect my goal. Otherwise, seek-and-flee.

                    Transform nearestTeamateToGkArea = GetTeammateBehindBallNearestTo(myGoal, true);
                    if (nearestTeamateToGkArea == self.transform)
                    {
                        // Protect your goal!

                        Vector2 gkPosition;
                        ComputeGkPosition(out gkPosition);

                        o_Axes = Seek(gkPosition, colliderRadius);

                        // If you can kick the ball away, try to do it.

                        float kickRadius = colliderRadius;
                        kickRadius += ballRadius;
                        kickRadius += 0.25f; // Safety threshold.

                        if (ballDistance < kickRadius)
                        {
                            o_KickButton = true;
                        }

                        SetBehaviourType_Internal("ProtectGoal");
                    }
                    else
                    {
                        SeekAndFleeBall(out o_Axes, m_MinFleeDistanceFactor, m_MaxFleeDistanceFactor, m_SeparationThreshold);

                        SetBehaviourType_Internal("SeekAndFlee");
                    }
                }
            }
            else
            {
                // You're a defender and you're not behind the ball. Is your ball inside gk area?

                if (IsNearToGoal(ballPosition))
                {
                    // Ball is inside my gk area. Check if you're the nearest player to the ball.

                    Transform nearestTeammate = GetTeammateNearestTo(ball, true);
                    if (nearestTeammate == self.transform)
                    {
                        // You're the nearest player to the ball. Return through your goal and recover your position.

                        Recover(myGoalPosition, out o_Axes, out o_KickButton, out o_DashButton, m_ForcedDashDistance);

                        SetBehaviourType_Internal("Recover");
                    }
                    else
                    {
                        // You aren't the nearest. Seek-and-flee.

                        SeekAndFleeBall(out o_Axes, m_MinFleeDistanceFactor, m_MaxFleeDistanceFactor, m_SeparationThreshold);

                        SetBehaviourType_Internal("SeekAndFlee");
                    }
                }
                else
                {
                    // Ball is far from your gk area. Return through your goal and recover your position.

                    Recover(myGoalPosition, out o_Axes, out o_KickButton, out o_DashButton, m_ForcedDashDistance);

                    SetBehaviourType_Internal("Recover");
                }
            }
        }
    }

    public class UpdateStrikerAction : Action
    {
        public SharedInputFiller filler;
        public SharedVector2 o_Axes;
        public SharedBool o_KickButton;
        public SharedBool o_DashButton;

        public SharedFloat m_ForcedDashDistance;
        public SharedFloat m_MinFleeDistanceFactor;
        public SharedFloat m_MaxFleeDistanceFactor;
        public SharedFloat m_SeparationThreshold;
        public SharedFloat m_RecoverTimer;
        public SharedFloat m_RecoverTimeThreshold;
        public SharedFloat m_DashDistance;
        public SharedFloat m_KickPrecision;

        public override TaskStatus OnUpdate()
        {
            Vector2 o_AxesTemp = (Vector2)o_Axes.GetValue();
            bool o_KickButtonTemp = (bool)o_KickButton.GetValue();
            bool o_DashButtonTemp = (bool)o_DashButton.GetValue();

            ((tnFaustoSavarinoAIInputFiller)filler.GetValue()).UpdateStriker_Internal(
                out o_AxesTemp, out o_KickButtonTemp, out o_DashButtonTemp,
                (float)m_ForcedDashDistance.GetValue(), (float)m_MinFleeDistanceFactor.GetValue(), (float)m_MaxFleeDistanceFactor.GetValue(),
                (float)m_SeparationThreshold.GetValue(), (float)m_RecoverTimer.GetValue(), (float)m_RecoverTimeThreshold.GetValue(),
                (float)m_DashDistance.GetValue(), (float)m_KickPrecision.GetValue());

            o_Axes.SetValue(o_AxesTemp);
            o_KickButton.SetValue(o_KickButtonTemp);
            o_DashButton.SetValue(o_DashButtonTemp);

            return TaskStatus.Success;
        }
    }

    private void UpdateStriker_Internal(out Vector2 o_Axes, out bool o_KickButton, out bool o_DashButton,
        float m_ForcedDashDistance, float m_MinFleeDistanceFactor, float m_MaxFleeDistanceFactor, float m_SeparationThreshold,
        float m_RecoverTimer, float m_RecoverTimeThreshold, float m_DashDistance, float m_KickPrecision)
    {
        o_Axes = Vector2.zero;

        o_KickButton = false;
        o_DashButton = false;

        // You're a striker.

        if (IsBallInMyHalfSide())
        {
            if (IsCharacterBehindBall(self))
            {
                // You're on my side and behind the ball, so move ahead to maintain an advanced position.

                Recover(opponentGoalPosition, out o_Axes, out o_KickButton, out o_DashButton, m_ForcedDashDistance);

                SetBehaviourType_Internal("Recover");
            }
            else
            {
                Vector2 strikerPosition = referencePosition;
                strikerPosition.x = midfield.x - (referencePosition.x - midfield.x);

                Vector2 axes = SeekPosition(strikerPosition, colliderRadius, m_MinFleeDistanceFactor, m_MaxFleeDistanceFactor, m_SeparationThreshold); // Ball is on your side and you're in an advanced position. WAIT.
                o_Axes = axes;

                SetBehaviourType_Internal("Wait");
            }
        }
        else
        {
            // The ball is on opponent side - your controlled area.

            if (!IsCharacterBehindBall(self))
            {
                // You're not behind the ball, so try to recover a useful position.

                Recover(myGoalPosition, out o_Axes, out o_KickButton, out o_DashButton, m_ForcedDashDistance);

                SetBehaviourType_Internal("Recover");
            }
            else
            {
                // You are behind the ball, so try to attack if you're the nearest player. Otherwise, seek-and-flee.

                Transform nearestTeammate = GetTeammateBehindBallNearestTo(ball, true);
                if (nearestTeammate == self.transform)
                {

                    ChargeBall(out o_Axes, out o_KickButton, out o_DashButton, m_RecoverTimer, m_RecoverTimeThreshold, m_DashDistance, m_KickPrecision);

                    SetBehaviourType_Internal("ChargeBall");
                }
                else
                {
                    SeekAndFleeBall(out o_Axes, m_MinFleeDistanceFactor, m_MaxFleeDistanceFactor, m_SeparationThreshold);

                    SetBehaviourType_Internal("SeekAndFlee");
                }
            }
        }
    }

    private void SeekAndFleeBall(out Vector2 o_Axes, float m_MinFleeDistanceFactor, float m_MaxFleeDistanceFactor, float m_SeparationThreshold)
    {
        Vector2 target;
        ComputeFieldPosition(out target, m_MinFleeDistanceFactor, m_MaxFleeDistanceFactor, m_SeparationThreshold);

        o_Axes = Seek(target, colliderRadius);
    }

    private void ComputeGkPosition(out Vector2 o_Position)
    {
        Vector2 gkPosition = GetFieldPosition(myGoalPosition);

        float idealHeight = ballPosition.y;

        Vector2 ballVelocity = GetVehicleVelocity(ball);
        if (ballVelocity.magnitude > 0.1f)
        {
            Vector2 ballDirection = ballVelocity.normalized;
            Vector2 ballToGoalDirection = GetMyGoalDirection(ball);

            float dot = Vector2.Dot(ballDirection, ballToGoalDirection);

            if (dot > 0f)
            {
                idealHeight = ballPosition.y + (gkPosition.x - ballPosition.x) * (ballVelocity.y / ballVelocity.x);
            }
        }

        float goalMinHeight = gkPosition.y - goalWidth / 2f;
        float goalMaxHeight = gkPosition.y + goalWidth / 2f;

        idealHeight = Mathf.Clamp(idealHeight, goalMinHeight, goalMaxHeight);

        if (gkPosition.x > midfield.x) // Right goal.
        {
            gkPosition.x -= colliderRadius;
            gkPosition.x -= 0.15f;
        }
        else // Left goal.
        {
            gkPosition.x += colliderRadius;
            gkPosition.x += 0.15f;
        }

        gkPosition.y = idealHeight;

        o_Position = gkPosition;
    }

    private void ChargeBall(out Vector2 o_Axes, out bool o_KickButton, out bool o_DashButton, float m_RecoverTimer, float m_RecoverTimeThreshold, float m_DashDistance, float m_KickPrecision)
    {
        o_Axes = Vector2.zero;

        o_KickButton = false;
        o_DashButton = false;

        Vector2 target = ballPosition;

        Vector2 direction = ballPosition - opponentGoalPosition;
        direction.Normalize();

        float offset = ballRadius;
        offset += colliderRadius;
        offset += 0.05f; // Tolerance threshold.

        target += direction * offset;

        target = ClampPosition(target, 0.2f);

        o_Axes = Seek(target);

        if (m_RecoverTimer > m_RecoverTimeThreshold)
        {
            o_KickButton = true; // Force kick to recover.
        }
        else
        {
            float targetDistance = Vector2.Distance(target, myPosition);

            if (targetDistance > m_DashDistance)
            {
                o_DashButton = true;
            }
            else
            {
                if (targetDistance < m_KickPrecision)
                {
                    o_KickButton = true;
                }
            }
        }
    }

    private Vector2 ClampPosition(Vector2 i_Position, float i_Tolerance = 0f)
    {
        // Check left.

        float leftThreshold = midfield.x - halfFieldWidth;
        leftThreshold += i_Tolerance;

        if (i_Position.x < leftThreshold)
        {
            i_Position.x = leftThreshold;
        }

        // Check right.

        float rightThreshold = midfield.x + halfFieldWidth;
        rightThreshold -= i_Tolerance;

        if (i_Position.x > rightThreshold)
        {
            i_Position.x = rightThreshold;
        }

        // Check top.

        float topThreshold = midfield.y + halfFieldHeight;
        topThreshold -= i_Tolerance;

        if (i_Position.y > topThreshold)
        {
            i_Position.y = topThreshold;
        }

        // Check bottom.

        float bottomThreshold = midfield.y - halfFieldHeight;
        bottomThreshold += i_Tolerance;

        if (i_Position.y < bottomThreshold)
        {
            i_Position.y = bottomThreshold;
        }

        return i_Position;
    }

    private Vector2 SeekPosition(Vector2 i_Position, float i_Tolerance, float m_MinFleeDistanceFactor, float m_MaxFleeDistanceFactor, float m_SeparationThreshold)
    {
        Vector2 target;
        ComputeFieldPosition(out target, m_MinFleeDistanceFactor, m_MaxFleeDistanceFactor, m_SeparationThreshold);

        target += i_Position;
        target *= 0.5f;

        Vector2 axes = Seek(target, i_Tolerance);
        return axes;
    }

    private void ComputeFieldPosition(out Vector2 o_Position, float m_MinFleeDistanceFactor, float m_MaxFleeDistanceFactor, float m_SeparationThreshold)
    {
        Vector2 target = Vector2.zero;

        // Compute sepration.

        {
            Vector2 separationTarget;
            ComputeSeparationTarget(out separationTarget, m_SeparationThreshold);

            target += separationTarget;
        }

        // Seek/Flee ball.

        {
            float fleeMinDistance = fieldHeight * m_MinFleeDistanceFactor;
            if (ballDistance < fleeMinDistance)
            {
                Vector2 seekTarget = myPosition;
                ComputeFleeTarget(ballPosition, fleeMinDistance, out seekTarget);

                target += seekTarget;
                target *= 0.5f;
            }
            else
            {
                float fleeMaxDistance = fieldHeight * m_MaxFleeDistanceFactor;
                if (ballDistance > fleeMaxDistance)
                {
                    Vector2 seekTarget = myPosition;
                    ComputeSeekTarget(ballPosition, fleeMaxDistance, out seekTarget);

                    target += seekTarget;
                    target *= 0.5f;
                }
            }
        }

        o_Position = target;
    }

    private void ComputeSeekTarget(Vector2 i_Source, float i_Threshold, out Vector2 o_Target)
    {
        Vector2 selfToSource = i_Source - myPosition;
        float distanceToSource = selfToSource.magnitude;

        Vector2 selfToSourceDirection = selfToSource.normalized;

        o_Target = myPosition + selfToSourceDirection * (distanceToSource - i_Threshold);
    }

    private void ComputeFleeTarget(Vector2 i_Source, float i_Threshold, out Vector2 o_Target)
    {
        Vector2 sourceToSelfDirection = myPosition - i_Source;
        sourceToSelfDirection.Normalize();

        o_Target = i_Source + sourceToSelfDirection * i_Threshold;
    }

    private void ComputeSeparationTarget(out Vector2 o_Target, float m_SeparationThreshold)
    {
        Vector2 sum = Vector2.zero;
        int count = 0;

        for (int teammateIndex = 0; teammateIndex < teammatesCount; ++teammateIndex)
        {
            Transform teammate = GetTeammateByIndex(teammateIndex);
            if (teammate != null)
            {
                Vector2 teammatePosition = teammate.position;
                Vector2 teammateToSelf = myPosition - teammatePosition;

                float distance = teammateToSelf.magnitude;

                if (distance < m_SeparationThreshold)
                {
                    Vector2 teammateToSelfDirection = teammateToSelf.normalized;

                    Vector2 target = teammatePosition + teammateToSelfDirection * m_SeparationThreshold;
                    sum += target;

                    ++count;
                }
            }
        }

        if (count > 0)
        {
            Vector2 separationTarget = sum / count;
            o_Target = separationTarget;
        }
        else
        {
            o_Target = myPosition;
        }
    }

    private void Recover(Vector2 i_Target, out Vector2 o_Axes, out bool o_KickButton, out bool o_DashButton, float m_ForcedDashDistance)
    {
        Vector2 targetDistance = i_Target - myPosition;

        Vector2 targetDirection = targetDistance.normalized;

        {
            // Apply direction correction in order to avoid the ball.

            Vector2 pointA = ballPosition;
            pointA.y += ballRadius;
            pointA.y += colliderRadius;
            pointA.y += 0.05f; // Safety tolerance.

            Vector2 pointB = ballPosition;
            pointB.y -= ballRadius;
            pointB.y -= colliderRadius;
            pointB.y -= 0.05f; // Safety tolerance.

            Vector2 selfToA = pointA - myPosition;
            Vector2 selfToB = pointB - myPosition;

            Vector2 directionA = selfToA.normalized;
            Vector2 directionB = selfToB.normalized;

            float angleA = Vector2.Angle(targetDirection, directionA);
            float angleB = Vector2.Angle(targetDirection, directionB);

            float angleAB = Vector2.Angle(directionA, directionB);

            if (angleA < angleAB && angleB < angleAB)
            {
                // Target direction is into ball fov. Apply a correction using the shortest line.

                if (selfToA.sqrMagnitude < selfToB.sqrMagnitude) // A is the nearest point.
                {
                    targetDirection = directionA;
                }
                else // B is the nearest point.
                {
                    targetDirection = directionB;
                }
            }
            else
            {
                // Target direction is fine. Nothing to do.
            }
        }

        o_Axes = targetDirection;

        o_KickButton = false;

        if (targetDistance.sqrMagnitude > m_ForcedDashDistance * m_ForcedDashDistance)
        {
            // Force dash movement, to recover your position quickly.

            o_DashButton = true;
        }
        else
        {
            o_DashButton = false;
        }
    }

    private void SetBehaviourMode_Internal(string i_Mode)
    {
        m_BT.SetVariableValue("behaviourMode", i_Mode);
    }

    private void SetBehaviourType_Internal(string i_Type)
    {
        m_BT.SetVariableValue("behaviourType", i_Type);
    }


    public class GetVehicleSpeedForTransform : Action
    {
        public SharedInputFiller filler;
        public SharedTransform vehicle;
        public SharedFloat storeResult;

        public override TaskStatus OnUpdate()
        {
            try
            {
                storeResult.SetValue(((tnFaustoSavarinoAIInputFiller)filler.GetValue()).GetVehicleSpeed((Transform)vehicle.GetValue()));
                return TaskStatus.Success;
            }
            catch (System.Exception e)
            {
                Debug.LogError(e.Message);
                return TaskStatus.Failure;
            }
        }
    }

    public class GetVehicleSpeedForGameObject : Action
    {
        public SharedInputFiller filler;
        public SharedGameObject vehicle;
        public SharedFloat storeResult;

        public override TaskStatus OnUpdate()
        {
            try
            {
                storeResult.SetValue(((tnFaustoSavarinoAIInputFiller)filler.GetValue()).GetVehicleSpeed((GameObject)vehicle.GetValue()));
                return TaskStatus.Success;
            }
            catch (System.Exception e)
            {
                Debug.LogError(e.Message);
                return TaskStatus.Failure;
            }
        }
    }

    public class UpdateRecoverTimer : Action
    {
        public SharedInputFiller filler;
        public SharedFloat recoverRadius;
        public SharedFloat recoverTimer;
        public SharedFloat frameTime;

        public override TaskStatus OnUpdate()
        {
            float ballSpeed = ((tnFaustoSavarinoAIInputFiller)filler.GetValue()).GetVehicleSpeed(((tnFaustoSavarinoAIInputFiller)filler.GetValue()).ball);
            float mySpeed = ((tnFaustoSavarinoAIInputFiller)filler.GetValue()).GetVehicleSpeed(((tnFaustoSavarinoAIInputFiller)filler.GetValue()).self);

            if (((tnFaustoSavarinoAIInputFiller)filler.GetValue()).ballDistance < ((float)recoverRadius.GetValue()) && (ballSpeed < 0.15f && mySpeed < 0.15f))
            {
                recoverTimer.SetValue(((float)recoverTimer.GetValue()) + ((float)frameTime.GetValue()));
            }
            else
            {
                recoverTimer.SetValue(0.0f);
            }
            return TaskStatus.Success;
        }
    }

    public class UpdateAttractTimer : Action
    {
        public SharedBool isAttracting;
        public SharedFloat attractTimer;
        public SharedFloat frameTime;

        public override TaskStatus OnUpdate()
        {
            if ((bool)isAttracting.GetValue())
            {
                attractTimer.SetValue((float)attractTimer.GetValue() + (float)frameTime.GetValue());
            }
            else
            {
                attractTimer.SetValue(0.0f);
            }
            return TaskStatus.Success;
        }
    }

    //[System.Serializable]
    //public enum BehaviourMode
    //{
    //    None,               // Grey
    //    Defender,           // Green
    //    Striker,            // Red
    //}

    //[System.Serializable]
    //public class SharedBehaviourMode : SharedVariable<BehaviourMode>
    //{
    //    public static implicit operator SharedBehaviourMode(BehaviourMode value) { return new SharedBehaviourMode { Value = value }; }
    //}

    //[System.Serializable]
    //public enum BehaviourType
    //{
    //    None,               // Grey
    //    Wait,               // White
    //    Recover,            // Yellow
    //    ChargeBall,         // Red
    //    SeekAndFlee,        // Green
    //    ProtectGoal,        // Blue
    //    Attract,            // Magenta
    //}

    //[System.Serializable]
    //public class SharedBehaviourType : SharedVariable<BehaviourType>
    //{
    //    public static implicit operator SharedBehaviourType(BehaviourType value) { return new SharedBehaviourType { Value = value }; }
    //}

    //[System.Serializable]
    //public enum AIRole
    //{
    //    Null = 0,
    //    Defender = 1,
    //    Midfielder = 2,
    //    Striker = 3,
    //}

    //[System.Serializable]
    //public class SharedAIRole : SharedVariable<AIRole>
    //{
    //    public static implicit operator SharedAIRole(AIRole value) { return new SharedAIRole { Value = value }; }
    //}

    private AIRole m_Role = AIRole.Null;

    // STATIC VARIABLES

    private static string s_Params = "Data/AI/StandardMatch/AIParams";

    // BT

    private BehaviorTree m_BT = null;

    // tnInputFiller's INTERFACE

    public override void Clear()
    {
        m_BT.SetVariableValue("behaviourMode", "None");
        m_BT.SetVariableValue("behaviourType", "None");

        m_BT.SetVariableValue("isAttracting", false);

        m_BT.SetVariableValue("kickCooldownTimer", 0.0f);
        m_BT.SetVariableValue("dashCooldownTimer", 0.0f);
        m_BT.SetVariableValue("tackleCooldownTimer", 0.0f);
        m_BT.SetVariableValue("attractCooldownTimer", 0.0f);

        m_BT.SetVariableValue("recoverTimer", 0.0f);

        m_BT.SetVariableValue("attractTimer", 0.0f);

        m_BT.SetVariableValue("axes", Vector2.zero);
        m_BT.SetVariableValue("axesSpeed", Vector2.zero);

        m_BT.SetVariableValue("inputFiller", this);
        m_BT.SetVariableValue("self", self);
        m_BT.SetVariableValue("ball", ball);
    }

    public override void Fill(float i_FrameTime, tnInputData i_Data)
    {
        if (!initialized || self == null)
        {
            ResetInputData(i_Data);
            return;
        }

        if (m_Role == AIRole.Null)
        {
            ResetInputData(i_Data);
            return;
        }

        // DO STUFF IN THE BT
        // ...

        // Fill input data.

        i_Data.SetAxis(InputActions.s_HorizontalAxis, ((Vector2)m_BT.GetVariable("axes").GetValue()).x);
        i_Data.SetAxis(InputActions.s_VerticalAxis, ((Vector2)m_BT.GetVariable("axes").GetValue()).y);

        i_Data.SetButton(InputActions.s_PassButton, (bool)m_BT.GetVariable("requestKick").GetValue());
        i_Data.SetButton(InputActions.s_ShotButton, (bool)m_BT.GetVariable("requestDash").GetValue());

        i_Data.SetButton(InputActions.s_AttractButton, (bool)m_BT.GetVariable("attract").GetValue());

    }

    // CTOR

    public tnFaustoSavarinoAIInputFiller(GameObject i_Self, AIRole i_Role, GameObject i_BT)
            : base(i_Self)
    {
        m_Role = i_Role;

        // BT

        m_BT = i_BT.GetComponent<BehaviorTree>();

        // INIT ROLE IN BT

        m_BT.SetVariableValue("aiRole", m_Role.ToString());

        // Setup parameters.

        tnStandardAIInputFillerParams aiParams = Resources.Load<tnStandardAIInputFillerParams>(s_Params);

        if (aiParams != null)
        {
            float floatOutValue;

            // Seek-and-flee behaviour.

            aiParams.GetMinFleeDistanceFactor(out floatOutValue);
            m_BT.SetVariableValue("m_MinFleeDistanceFactor", floatOutValue);
            aiParams.GetMaxFleeDistanceFactor(out floatOutValue);
            m_BT.SetVariableValue("m_MaxFleeDistanceFactor", floatOutValue);

            // Separation.

            aiParams.GetSeparationThreshold(out floatOutValue);
            m_BT.SetVariableValue("m_SeparationThreshold", floatOutValue);

            // Energy thresholds.

            aiParams.GetMinDashEnergy(out floatOutValue);
            m_BT.SetVariableValue("m_MinDashEnergy", floatOutValue);
            aiParams.GetMinKickEnergy(out floatOutValue);
            m_BT.SetVariableValue("m_MinKickEnergy", floatOutValue);
            aiParams.GetMinTackleEnergy(out floatOutValue);
            m_BT.SetVariableValue("m_MinTackleEnergy", floatOutValue);
            aiParams.GetMinAttractEnergy(out floatOutValue);
            m_BT.SetVariableValue("m_MinAttractEnergy", floatOutValue);

            // Cooldown timers.

            aiParams.GetDashCooldown(out floatOutValue);
            m_BT.SetVariableValue("m_DashCooldown", floatOutValue);
            aiParams.GetKickCooldown(out floatOutValue);
            m_BT.SetVariableValue("m_KickCooldown", floatOutValue);
            aiParams.GetTackleCooldown(out floatOutValue);
            m_BT.SetVariableValue("m_TackleCooldown", floatOutValue);
            aiParams.GetAttractCooldown(out floatOutValue);
            m_BT.SetVariableValue("m_AttractCooldown", floatOutValue);

            // Dash behaviour.

            aiParams.GetDashDistance(out floatOutValue);
            m_BT.SetVariableValue("m_DashDistance", floatOutValue);
            aiParams.GetForcedDashDistance(out floatOutValue);
            m_BT.SetVariableValue("m_ForcedDashDistance", floatOutValue);

            // Kick behaviour.

            aiParams.GetKickPrecision(out floatOutValue);
            m_BT.SetVariableValue("m_KickPrecision", floatOutValue);

            // Tackle behaviour.

            aiParams.GetTackleRadius(out floatOutValue);
            m_BT.SetVariableValue("m_TackleRadius", floatOutValue);
            aiParams.GetBallDistanceThreshold(out floatOutValue);
            m_BT.SetVariableValue("m_BallDistanceThreshold", floatOutValue);

            // Attract behaviour.

            aiParams.GetAttractMinRadius(out floatOutValue);
            m_BT.SetVariableValue("m_AttractMinRadius", floatOutValue);
            aiParams.GetAttractMaxRadius(out floatOutValue);
            m_BT.SetVariableValue("m_AttractMaxRadius", floatOutValue);

            aiParams.GetAttractTimeThreshold(out floatOutValue);
            m_BT.SetVariableValue("m_AttractTimeThreshold", floatOutValue);

            // Extra parameters.

            aiParams.GetRecoverRadius(out floatOutValue);
            m_BT.SetVariableValue("m_RecoverRadius", floatOutValue);
            aiParams.GetRecoverTimeThreshold(out floatOutValue);
            m_BT.SetVariableValue("m_RecoverTimeThreshold", floatOutValue);
        }
    }

    protected bool IsCharacterBehindBall_Internal(Transform i_Character)
    {
        if (i_Character == null)
        {
            return false;
        }

        if (ball == null || myGoal == null)
        {
            return false;
        }

        Vector2 ballPosition = ball.transform.position;
        Vector2 myGoalPosition = myGoal.transform.position;

        float borderThreshold = GetColliderRadius(i_Character);
        borderThreshold += 0.5f; // Safety threshold.

        if (myGoalPosition.x > 0f)
        {
            return (i_Character.position.x > ballPosition.x) || (bottomRight.x - i_Character.position.x < borderThreshold);
        }
        else
        {
            return (i_Character.position.x < ballPosition.x) || (i_Character.position.x - topLeft.x < borderThreshold);
        }
    }

}
